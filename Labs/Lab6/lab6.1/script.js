"use strict";

const tamp1 = [17, 21, 23];
const tamp2 = [12, 5, -5, 0, 4];

function printForecast(arr) {
  for (let i = 0; i < arr.length; i++) {
    console.log(`${arr[i]} ºC in ${i + 1} day`);
  }
}

printForecast(tamp1);
console.log("-----------------");
printForecast(tamp2);
