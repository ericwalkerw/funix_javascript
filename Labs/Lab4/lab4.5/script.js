const worldPopulation = 7900000000;
const vietnamPopulation = 100000000;
const finlandPopulation = 6000000;
const chinaPopulation = 1441000000;
const canadaPopulation = 18000000;

const percentageOfWorld = (population) => (population / worldPopulation) * 100;

const populations = [
  vietnamPopulation,
  finlandPopulation,
  chinaPopulation,
  canadaPopulation,
];

const percentages = [
  percentageOfWorld(vietnamPopulation).toFixed(2) + " %",
  percentageOfWorld(finlandPopulation).toFixed(2) + " %",
  percentageOfWorld(chinaPopulation).toFixed(2) + " %",
  percentageOfWorld(canadaPopulation).toFixed(2) + " %",
];

console.log(populations);
console.log(percentages);
