const WorldPopulation = 7900000000;

function percentageOfWorld(population) {
  return (population / WorldPopulation) * 100;
}

const chinaPopulation = percentageOfWorld(1441000000);
const vietnamPopulation = percentageOfWorld(100000000);
const FinlandPopulation = percentageOfWorld(6000000);

console.log(`China population percent: ${chinaPopulation.toFixed(2)} %`);
console.log(`vietnam population percent: ${vietnamPopulation.toFixed(2)} %`);
console.log(`Finland population percent: ${FinlandPopulation.toFixed(2)} %`);
