const calcAverage = (a, b, c) => (a + b + c) / 3;

const avgDolphins = calcAverage(44, 23, 71);
const avgKoalas = calcAverage(65, 54, 49);

const avgDolphins2 = calcAverage(85, 54, 41);
const avgKoalas2 = calcAverage(23, 34, 27);

function CheckWinner(avgDolphins, avgKoalas) {
  if (avgDolphins >= 2 * avgKoalas) {
    console.log(`Dolphins win : ${avgDolphins} vs ${avgKoalas}`);
  } else if (avgKoalas >= 2 * avgDolphins) {
    console.log(`Koalas win : ${avgKoalas} vs ${avgDolphins}`);
  } else {
    console.log(`No team win : ${avgDolphins} vs ${avgKoalas}`);
  }
}

CheckWinner(avgDolphins, avgKoalas);
CheckWinner(avgDolphins2, avgKoalas2);
