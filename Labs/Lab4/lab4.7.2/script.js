const calcTip = (bill) =>
  bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;

const bills = [125, 555, 44];

const totalBill = (bill) => calcTip(bill) + bill;

for (let i = 0; i < bills.length; i++) {
  console.log(
    `Tip : ${calcTip(bills[i])} $ - Total bill : ${totalBill(bills[i])} $`
  );
}
