const WorldPopulation = 7900000000;

function describePopulation(country, population) {
  return `${country} has ${
    population / 1000000
  } million people, which is about ${(
    (population / WorldPopulation) *
    100
  ).toFixed(2)} % of the world'.`;
}

const chinaPopulation = describePopulation("china", 1441000000);
const vietnamPopulation = describePopulation("Vietnam", 100000000);
const finlandPopulation = describePopulation("FinLand", 6000000);

console.log(chinaPopulation);
console.log(vietnamPopulation);
console.log(finlandPopulation);
