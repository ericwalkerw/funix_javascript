const WorldPopulation = 7900000000;

const percentageOfWorld3 = (population) => (population / WorldPopulation) * 100;

const chinaPopulation = percentageOfWorld3(1441000000);
const vietnamPopulation = percentageOfWorld3(100000000);
const FinlandPopulation = percentageOfWorld3(6000000);

console.log(`China population percent: ${chinaPopulation.toFixed(2)} %`);
console.log(`vietnam population percent: ${vietnamPopulation.toFixed(2)} %`);
console.log(`Finland population percent: ${FinlandPopulation.toFixed(2)} %`);
