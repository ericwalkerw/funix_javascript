"use strict";

let hasDriversLicense = false;
const passTest = true;

if (passTest) hasDriversLicense = true;
if (hasDriversLicense) console.log("I can drive");

function describeCountry(country, population, capitalCity) {
  return `${country} has ${population} million people and its capital city is ${capitalCity}`;
}

const vietnam = describeCountry("Vietnam", 100000000, "HoChiMinhCity");
const finland = describeCountry("Finland", 6000000, "finlandCity");
const canada = describeCountry("Canada", 38000000, "TorontoCity");

console.log(`${vietnam}, 
${finland},
${canada}`);
