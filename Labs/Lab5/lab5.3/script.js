const myCountry = {
  country: "Finland",
  capital: "Helsinki",
  language: "Finnish",
  population: 6,
  neighbours: ["neighbour_1", "neighbour_2", "neighbour_3"],

  isIsLand: false,

  describe: function () {
    console.log(`
    ${this.country} has ${this.population} million ${this.language}-speaking people, ${this.neighbours.length} neighbouring countries and a capital called ${this.capital}
    `);
  },

  checkIsland: function () {
    this.isIsLand = this.neighbours.length === 0 ? true : false;

    this.isIsLand
      ? console.log(`${this.country} has isLand`)
      : console.log(`${this.country} has no isLand`);
  },
};

myCountry.describe();
myCountry.checkIsland();

console.log(myCountry);
