const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];
const tips = [];
const totals = [];

const calcTip = (bill) =>
  bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;

let sum = 0;
function calcAverage(arr) {
  for (let i = 0; i < arr.length; i++) {
    sum += Number(arr[i]);
  }
}

for (let i = 0; i < bills.length; i++) {
  const tip = calcTip(bills[i]);
  tips.push(tip.toFixed(2));
  totals.push((bills[i] + tip).toFixed(2));
}

console.log(tips);
console.log(totals);

calcAverage(totals);
console.log(sum / totals.length);
