const finland = {
    country: "Finland",
    capital: "Helsinki",
    language: "Finnish",
    population: 6,
    neighbours: ["Sweden", "Norway", "Russia"],

    description : function(){
        return `${this.country} has ${this.population} million ${this.language}-speaking people, ${this.neighbours.length-1} neighbouring countries and a capital called ${this.capital}.`
    }
}
console.log(finland.description());

finland.population += 2;
console.log(finland.description());

finland["population"] -= 2;
console.log(finland.description());