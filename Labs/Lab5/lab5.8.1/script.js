const person = {
  fullName: "",
  mass: 0,
  height: 0,

  calcBMI() {
    this.bmi = this.mass / this.height ** 2;
    return this.bmi;
  },
};

const mark = { ...person };
mark.fullName = "Mark Miller";
mark.mass = 78;
mark.height = 1.69;
mark.calcBMI();

const john = { ...person };
john.fullName = "John Smith";
john.mass = 92;
john.height = 1.95;
john.calcBMI();

console.log(
  `${mark.fullName}'s BMI (${mark.bmi.toFixed(1)}) is ${
    mark.bmi > john.bmi ? "higher" : "lower"
  } than ${john.fullName}'s (${john.bmi.toFixed(1)})!`
);
