const massMark = 95,
  heightMark = 1.88;
const massJohn = 85,
  heightJohn = 1.76;

const BMIMark = massMark / heightMark ** 2;
const BMIJohn = massJohn / (heightJohn * heightJohn);

const markHightBMI = BMIMark > BMIJohn;

if (markHightBMI) {
  console.log(
    `Mark BMI ${BMIMark.toFixed(1)} is higher than John BMI ${BMIJohn.toFixed(
      1
    )}`
  );
} else
  console.log(
    `Mark BMI ${BMIMark.toFixed(1)} is shoter than John BMI ${BMIJohn.toFixed(
      1
    )}`
  );
