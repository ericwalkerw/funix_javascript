const vietnam = createCountry(
  "Vietnam",
  "Asia",
  "Vietnamese",
  false,
  100000000
);
const averageCountry = createCountry(
  "Average Country",
  "Any",
  "Any",
  true,
  33000000
);
const finland = createCountry("Finland", "Europe", "Finnish", true, 6000000);

function formatPopulation(population) {
  if (population < 0) return population * -1;
  if (population >= 1000000000) {
    return (population / 1000000000).toFixed(1) + " B";
  } else if (population >= 1000000) {
    return (population / 1000000).toFixed(1) + " M";
  } else if (population >= 1000) {
    return (population / 1000).toFixed(1) + " K";
  } else {
    return population.toString();
  }
}

function createCountry(name, continent, language, isIsland, population) {
  const formattedPopulation = population;
  const description = `${name} and its ${formatPopulation(
    formattedPopulation
  )} people speak ${language}`;
  return {
    Name: name,
    Continent: continent,
    Language: language,
    isIsland,
    Population: formattedPopulation,
    description,
  };
}

function printCountryInfo(country) {
  console.log(country);
  console.log("N/S population : ", formatPopulation(country.Population / 2));
  console.log(
    `${country.Name}'s Population:`,
    formatPopulation(country.Population)
  );
  console.log(`${country.Name}'s Description:`, country.description);
  console.log(ComparePopulation(country));
}

function ComparePopulation(country) {
  const populationCompare = country.Population - averageCountry.Population;
  if (country.Population > averageCountry.Population) {
    return `${country.Name} has a population above average ${formatPopulation(
      populationCompare
    )}`;
  } else if (country.Population < averageCountry.Population) {
    return `${country.Name} has a population below average ${formatPopulation(
      populationCompare
    )}`;
  } else {
    return `${country.Name} has the same population as the average`;
  }
}

printCountryInfo(vietnam);
printCountryInfo(finland);
