const Dolphins = (96 + 108 + 89) / 3;
const Koalas = (88 + 91 + 110) / 3;
console.log(Dolphins.toFixed(1), " vs ", Koalas.toFixed(1));

if (Dolphins > Koalas) {
  console.log("Dolphins win the trophy : ", Dolphins.toFixed(1));
} else if (Dolphins < Koalas) {
  console.log("Koalas win the trophy : ", Koalas.toFixed(1));
} else {
  console.log("both win the trophy");
}
