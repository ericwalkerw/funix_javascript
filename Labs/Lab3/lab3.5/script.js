const population = 3300000; // Thay đổi giá trị này để kiểm tra kết quả

const result =
  population > 33000000
    ? "Portugal's population is above average"
    : "Portugal's population is below average";

console.log(result);
