const countryName = prompt("Enter the name of the country:");
const countryLanguage = prompt("Enter the language spoken in the country:");
const countryPopulation = prompt("Enter the population of the country:");
const isIsland = prompt("Is the country on an island? (yes/no)") == "yes";

const checkCountry =
  countryLanguage == "english" && countryPopulation < 50000000 && !isIsland;

if (checkCountry) {
  console.log(`You should live in ${countryName} :)`);
} else {
  console.log(`${countryName} does not meet your criteria :(`);
}
