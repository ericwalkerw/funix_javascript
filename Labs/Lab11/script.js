"use strict";

const anserButton = document.getElementById("answerButton");
const poll = {
  question: "What is your favourite programming language? ",
  options: ["0: JavaScript", "1: Python", "2: Rust", "3: C++"],
  numberOfVotes: new Array(4).fill(0),
};

function registerNewAnswer() {
  const answer = prompt(
    `${poll.question}\n${poll.options.join("\n")}\n(Write option number)`
  );
  const answerIndex = parseInt(answer);

  if (answer === null) {
    return; // User canceled
  }

  if (
    !isNaN(answerIndex) &&
    answerIndex >= 0 &&
    answerIndex < poll.numberOfVotes.length
  ) {
    poll.numberOfVotes[answerIndex]++;
  } else {
    alert("Câu trả lời không hợp lệ");
  }

  console.clear();
  displayResults("array");
  displayResults("string");
}

function displayResults(type = "array") {
  if (type === "array") {
    console.log(poll.numberOfVotes);
  } else if (type === "string") {
    console.log(`Poll results are ${poll.numberOfVotes.join(", ")}`);
  }
}

anserButton.addEventListener("click", registerNewAnswer);
