"use strict";
/**
 *Bắt sự kiện Click vào nút "Submit"
 *Lấy dữ liệu từ các Form Input
 *Validate dữ liệu
 *Thêm thú cưng vào danh sách
 *Hiển thị danh sách thú cưng
 *Xóa các dữ liệu nhập trong Form Input
 *Show Health pet
 */

//#region Variables
const idInput = document.getElementById("input-id");
const nameInput = document.getElementById("input-name");
const ageInput = document.getElementById("input-age");
const typeInput = document.getElementById("input-type");
const weightInput = document.getElementById("input-weight");
const lengthInput = document.getElementById("input-length");
const colorInput = document.getElementById("input-color-1");
const breedInput = document.getElementById("input-breed");
const vaccinatedInput = document.getElementById("input-vaccinated");
const dewormedInput = document.getElementById("input-dewormed");
const sterilizedInput = document.getElementById("input-sterilized");

const submitBtn = document.getElementById("submit-btn");
const healthyBtn = document.getElementById("healthy-btn");
const tableBodyEl = document.getElementById("tbody");
const formatDate = (date) => date.toLocaleDateString();
let petArr = JSON.parse(getFromStorage("petArr")) ?? "[]";
let isClick = false;
//#endregion

//#region testcase
const testcase1 = {
  id: 1,
  name: "test",
  age: 2,
  type: "dog",
  weight: 12,
  length: 22,
  breed: "vietnam",
  color: "green",
  vaccinated: true,
  dewormed: false,
  sterilized: false,
  date: new Date(),
};

const testcase2 = {
  id: 2,
  name: "test2",
  age: 1,
  type: "cat",
  weight: 4,
  length: 13,
  breed: "America",
  color: "yellow",
  vaccinated: true,
  dewormed: true,
  sterilized: true,
  date: new Date(),
};

petArr.push(testcase1);
petArr.push(testcase2);
renderTableData(petArr);
console.log(petArr);
//#endregion

//#region Events
submitBtn.addEventListener("click", submitClicked);

healthyBtn.addEventListener("click", healthClicked);
//#endregion

//#region function
function healthClicked() {
  isClick = !isClick;
  if (isClick) {
    const healthResult = petArr.filter(
      (p) => p.vaccinated && p.dewormed && p.sterilized
    );
    renderTableData(healthResult);
    healthyBtn.textContent = "Show All Pets";
  } else {
    healthyBtn.textContent = "Show Health Pets";
    renderTableData(petArr);
  }
}

function submitClicked() {
  const data = {
    id: parseInt(idInput.value),
    age: parseInt(ageInput.value),
    weight: parseInt(weightInput.value),
    length: parseInt(lengthInput.value),
    name: nameInput.value,
    type: typeInput.value,
    color: colorInput.value,
    breed: breedInput.value,
    vaccinated: vaccinatedInput.checked,
    dewormed: dewormedInput.checked,
    sterilized: sterilizedInput.checked,
    date: new Date(),
  };

  const validate = validateData(data);

  if (validate) {
    petArr.push(data);
    clearInput();
    renderTableData(petArr);
  }
}

function renderTableData(arr) {
  tableBodyEl.innerHTML = "";

  arr.forEach((pet) => {
    const row = document.createElement("tr");
    row.innerHTML = `
  <td>P00${pet.id}</td>
  <td>${pet.name}</td>
  <td>${pet.age}</td>
  <td>${pet.type}</td>
  <td>${pet.weight}</td>
  <td>${pet.length}</td>
  <td>${pet.breed}</td>
  <td><span class="color" style="background-color:${pet.color}"></span>
  </td>
  <td class="${renderIcon(pet.vaccinated)}"></td>
  <td class="${renderIcon(pet.dewormed)}"></td>
  <td class="${renderIcon(pet.sterilized)}"></td>
  <td>${formatDate(pet.date)}</td>
  <td>
	<button class="btn btn-danger" onclick="deletePet('${pet.id}')">Delete</button>
</td>
  `;
    tableBodyEl.appendChild(row);
  });
}

function deletePet(id) {
  if (confirm("Are you sure you want to delete!")) {
    const result = petArr.filter((p) => p.id != id);
    petArr = result;
    renderTableData(result);
  }
}

function validateData(data) {
  const showAlert = (message) => (alert(message), false);
  const isInvalid = (value, min, max) =>
    isNaN(value) || value < min || value > max;

  if (
    isInvalid(data.id, 1, Infinity) ||
    petArr.some((pet) => pet.id === data.id)
  )
    return showAlert(`Cannot be empty \nID must be unique!`);

  if (!data.name.trim()) return showAlert("Name cannot be empty");

  if (isInvalid(data.age, 1, 15))
    return showAlert(`Cannot be empty \nAge must be between 1 and 15!`);

  if (data.type === "Select Type") return showAlert("Please select a type");

  if (isInvalid(data.weight, 1, 15))
    return showAlert(`Cannot be empty \nWeight must be between 1 and 15!`);

  if (isInvalid(data.length, 1, 100)) {
    return showAlert(`Cannot be empty \nLength must be between 1 and 100!`);
  }

  if (data.breed === "Select Breed") return showAlert("Please select a breed");

  return true;
}

const clearInput = () => {
  idInput.value = "";
  nameInput.value = "";
  ageInput.value = "";
  typeInput.value = "Select Type";
  weightInput.value = "";
  lengthInput.value = "";
  colorInput.value = "#000000";
  breedInput.value = "Select Breed";
  vaccinatedInput.checked = false;
  dewormedInput.checked = false;
  sterilizedInput.checked = false;
};

function renderIcon(value) {
  if (value) {
    return "bi bi-check-circle-fill";
  } else {
    return "bi bi-x-circle-fill";
  }
}
//#endregion
