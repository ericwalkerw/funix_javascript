"use strict";
import FORMATDATE from "./formatDate.js";
const $ = document.getElementById.bind(document);
const $$ = document.querySelectorAll.bind(document);
/**
 * Bắt sự kiện Click vào nút "Submit"
 *Lấy dữ liệu từ các Form Input
 *Validate dữ liệu
 *Thêm thú cưng vào danh sách
 *Hiển thị danh sách thú cưng
 *Xóa các dữ liệu nhập trong Form Input
 */

//#region HTML elements
const submitBtn = $("submit-btn");
const healthyBtn = $("healthy-btn");
const BMIBtn = $("BMI-btn");
const tableBodyEl = $("tbody");

const idInput = $("input-id");
const ageInput = $("input-age");
const typeInput = $("input-type");
const nameInput = $("input-name");
const breedInput = $("input-breed");
const colorInput = $("input-color-1");
const weightInput = $("input-weight");
const lengthInput = $("input-length");
const dewormedInput = $("input-dewormed");
const vaccinatedInput = $("input-vaccinated");
const sterilizedInput = $("input-sterilized");

const inputEls = $$("input");
//#endregion
let petArr = [];
let isOpenFullList = true;

//#region Functions
function clearInput() {
  for (const input of inputEls) {
    if (input.type === "text" || input.type === "number") {
      input.value = "";
    } else if (input.type === "checkbox") {
      input.checked = false;
    }
    typeInput.value = "Select Type";
    breedInput.value = "Select Breed";
  }
}

function validateInput() {
  const id = idInput.value.trim();
  const age = parseInt(ageInput.value);
  const weight = parseFloat(weightInput.value);
  const length = parseFloat(lengthInput.value);
  const type = typeInput.value;
  const breed = breedInput.value;

  if (
    !id ||
    !age ||
    !weight ||
    !length ||
    type === "Select Type" ||
    breed === "Select Breed"
  ) {
    throw new Error("Please fill in all the required fields.");
  }

  if (age < 1 || age > 15) {
    throw new Error("Age must be between 1 and 15.");
  }

  if (weight < 1 || weight > 15) {
    throw new Error("Weight must be between 1 and 15.");
  }

  if (length < 1 || length > 100) {
    throw new Error("Length must be between 1 and 100.");
  }

  return true;
}

function CreateTable(datas) {
  tableBodyEl.innerHTML = "";
  datas.forEach((element) => {
    const row = document.createElement("tr");
    row.innerHTML = `
    <td>${element.id}</td>
    <td>${element.name}</td>
    <td>${element.age}</td>
    <td>${element.type}</td>
    <td>${element.weight} Kg</td>
    <td>${element.length} Cm</td>
    <td>${element.breed}</td>
    <td><span style="width: 36px; height: 12px; background-color: ${
      element.color
    }"></span></td>
    <td><i class="${RenderCheck(element.vaccinated)}"></i></td>
    <td><i class="${RenderCheck(element.dewormed)}"></td>
    <td><i class="${RenderCheck(element.sterilized)}"></td>
    <td>${element.BMI}</td>
    <td>${FORMATDATE(element.date)}</td>
    <td>
        <button class="btn btn-danger" onclick="deletePet('${
          element.id
        }')">Delete</button>   
    </td>`;

    tableBodyEl.appendChild(row);
  });
}

function RenderCheck(bool) {
  return bool ? "bi bi-check-circle-fill" : "bi bi-x-circle-fill";
}

function deletePet(idRemove) {
  // if (confirm("Are you sure you want to delete this pet?")) {
  //   petArr = petArr.filter((value) => value.id !== idRemove);
  //   CreateTable(petArr);
  // }
  console.log("alo");
}

function ClickedSubmit(e) {
  e.preventDefault();
  try {
    //validateInput();

    const data = {
      id: idInput.value.trim(),
      name: nameInput.value.trim(),
      age: parseInt(ageInput.value),
      type: typeInput.value,
      weight: parseFloat(weightInput.value),
      length: parseFloat(lengthInput.value),
      breed: breedInput.value,
      color: colorInput.value,
      vaccinated: vaccinatedInput.checked,
      dewormed: dewormedInput.checked,
      sterilized: sterilizedInput.checked,
      BMI: 0,
      date: new Date(),
    };

    petArr.push(data);
    CreateTable(petArr);
    clearInput();
  } catch (error) {
    alert(error.message);
  }
}

function ClickedShowHealth() {
  isOpenFullList = !isOpenFullList;
  if (isOpenFullList) {
    CreateTable(petArr);
  } else {
    const HealthList = petArr.filter(
      (value) => value.dewormed && value.sterilized && value.vaccinated
    );
    CreateTable(HealthList);
  }
}

function ClickedCalculateBMI() {
  petArr.forEach((value) => {
    if (value.type === "Dog") {
      value.BMI = ((value.weight * 703) / value.length ** 2).toFixed(2);
    } else if (value.type === "Cat") {
      value.BMI = ((value.weight * 886) / value.length ** 2).toFixed(2);
    } else {
      value.BMI = 0;
    }
  });
  CreateTable(petArr);
}
//#endregion

const App = {
  HandlesEvent() {
    submitBtn.addEventListener("click", ClickedSubmit);
    healthyBtn.addEventListener("click", ClickedShowHealth);
    BMIBtn.addEventListener("click", ClickedCalculateBMI);
  },
  Start() {
    this.HandlesEvent();
  },
};

App.Start();
