const $ = document.querySelector.bind(document);
var courses = ["HTML & CSS", "Javascript", "PHP", "Java"];
const ulEl = $("ul");

function render(courses) {
  var newMap = courses.map((course) => {
    return `<li>${course}</li>`;
  });
  ulEl.innerHTML = `${newMap.join("")}`;
}

render(courses);
const divEl = document.querySelectorAll("div");

const Courses = function (a, b) {
  this.a = a;
  this.b = b;
};
